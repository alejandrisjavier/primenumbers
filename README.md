# IT4W Prueba Técnica

Este repositorio contiene la prueba técnico de IT4W, creado por Javier Alejandris

# Descripción de la aplicación

La aplicación funciona sobre una ventana de escritorio

La pantalla principal posee un cuadro de texto donde se puede ingresar cualquier texto que incluya numeros,
tambien posee un botón de ejecucion el cual procesa el texto ingresado y muestra el resultado el cuadro de texto 

El cuadro de resultados muestra la lista números primos previamente procesado.


# Detalles técnicos

* La aplicación utiliza .Net Framwork 4.7.2
* La aplicación fue desatollada en Windows Forms en su totalidad en C#
* El Proyecto utiliza el patron MVP
* El Proyecto usa principios SOLID


# Librerías utilizadas

* Las librerías estándar (.Net)
* La librería using System.Text.RegularExpressions;

La elección de las librerías de Expresiones regulares se utiliza para obtener solo los numeros de un texto.


# Ejecucion:

* La ejecucion se realiza desde el menu Depurar->Iniciar Depueracion o con el teclado F5


# Notas:

* El código esta comentado sobre los metodos y lineas que se consideraron necesarias.

* Si ocurre un error que afecta la experiencia del usuario, este se vera reflejado en forma de Popup en la pantalla.
