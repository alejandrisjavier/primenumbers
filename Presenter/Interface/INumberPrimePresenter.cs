﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeNumbers.Presenter
{
    internal interface INumberPrimePresenter
    {
        void primeNumberPrinter(string texto);
        string[] getNumbers(string texto);
        bool isPrime(long val);
    }
}
