﻿using PrimeNumbers.ui.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrimeNumbers.Presenter
{
    internal class NumberPrimePresenter : INumberPrimePresenter
    {

        IfrmPrimeNumberView _view;
        public NumberPrimePresenter(IfrmPrimeNumberView view)
        {
            _view = view;
        }


        /// <summary>
        /// Recibe un texto y retorna una array de long que contiene los números primos
        /// </summary>
        /// <param name="texto"></param>
        /// <returns></returns>
        public void primeNumberPrinter(string texto)
        {

            _view.showprogress(true);
            string[] results = getNumbers(texto);

            //'elementos' es el recolector de numeros primos y el que luego va a devolver la funcion 
            List<long> elementos = new List<long>();


            string strvalue = string.Empty;
            long valueparse;
            string stringnoleft;

            //recorre el array de string que contiene números
            foreach (string number in results)
            {

                strvalue = string.Empty;
                //recorre cada caracter que contiene los números
                foreach (char num in number)
                {


                    try
                    {
                        //1-valido el número (strvalue) si es primo y lo agrego a la lista de elementos
                        strvalue += num.ToString();
                        valueparse = long.Parse(strvalue);
                        if (isPrime(valueparse) && !elementos.Contains(valueparse))
                            elementos.Add(valueparse);

                    }
                    catch(Exception ex)
                    {
                        _view.showmessage(ex.Message);
                    }

                    //2-recorre el numero string quitando cada caracter izquierdo, valida si es primo
                    //y lo agrega a los elementos
                    for (int x = 1; x < strvalue.Length; x++)
                    {
                        try
                        {
                           // stringnoleft = strvalue.Substring(x, strvalue.Length - x);
                            stringnoleft = strvalue.Substring(x, strvalue.Length - x);

                            valueparse = long.Parse(stringnoleft);

                            if (isPrime(valueparse) && !elementos.Contains(valueparse))
                                elementos.Add(valueparse);

                        }
                        catch (Exception ex)
                        {
                            _view.showmessage(ex.Message);
                        }

                }
                }
            }
            
            _view.showvalues("[" + String.Join(",", elementos) + "]");
            _view.showprogress(false);

        }

        /// <summary>
        /// Retorna un array de string que contiene solo los números del texto recibido como parametro
        /// </summary>
        /// <param name="texto"></param>
        /// <returns></returns>
        public string[] getNumbers(string texto)
        {
            //patron es para obter solo los numeros del string
            string patron = @"\d+";
            //string operacion = texto;
            Regex regex = new Regex(patron);

            //Matches Recuperar todas las coincidencias del patron de la cadena de 'texto'
            //y se guardan en el array de string resultados
            string[] resultados = regex.Matches(texto)
                           .OfType<Match>()
                           .Select(m => m.Value)
                           .ToArray();

            return resultados;
        }

        /// <summary>
        /// Verifica si el número recibido por parametro es Primo
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public bool isPrime(long val)
        {
            if (val <= 1)
                return false;

            for (int i = 2; i < val; i++)
            {
                if (val % i == 0)
                    return false;
            }

            return true;
        }
    }
}
