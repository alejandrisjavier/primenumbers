﻿using PrimeNumbers.Presenter;
using PrimeNumbers.ui.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrimeNumbers
{
    public partial class frmPrimeNumber : Form, IfrmPrimeNumberView
    {

        INumberPrimePresenter presenter;
        public frmPrimeNumber()
        {
            InitializeComponent();
            setpresenter();
        }

        public void setpresenter()
        {
            presenter = (new NumberPrimePresenter(this));
        }
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            presenter.primeNumberPrinter(txt_valor.Text);
        }

        public void showvalues(string values)
        {
            this.txt_result.Text = values.ToString();
        }

        public void showmessage(string message)
        {
            MessageBox.Show(message);
        }

        public void showprogress(bool progress)
        {
            this.Cursor = progress ? Cursors.WaitCursor : Cursors.Default;

        }
    }
}
