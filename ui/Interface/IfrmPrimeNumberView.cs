﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeNumbers.ui.Interface
{
    internal interface IfrmPrimeNumberView
    {
        void setpresenter();
        void showvalues(string values);
        void showmessage(string message);

        void showprogress(Boolean progress);
    }
}
